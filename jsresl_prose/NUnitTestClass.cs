﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace jsresl_prose
{
    public class NUnitTestClass
    {

        [Test()]
        public void TestCase00()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "send email to json@acme.com gh ijk lmn" });
            inputs.Add(new List<Object> { "contact help@robot.com abc def" });
            inputs.Add(new List<Object> { "email me: wslee@ropas.snu.ac.kr please" });
            outputs.Add("json@acme.com");
            outputs.Add("help@robot.com");
            outputs.Add("wslee@ropas.snu.ac.kr");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }



        
        //11604909.sl
        //처음 나오는 숫자.숫자 형식을 가지고 있는 맨처음에 나오는 것 출력
        [Test()]
        public void TestCase01()
        {

            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();

            inputs.Add(new List<Object> { "AIX 5.1" });
            outputs.Add("5.1");
            inputs.Add(new List<Object> { "VMware ESX Server 3.5.0 build-110268" });
            outputs.Add("3.5");
            inputs.Add(new List<Object> { "Linux Linux 2.6 Linux" });
            outputs.Add("2.6");
            inputs.Add(new List<Object> { "Red Hat Enterprise AS 4 <2.6-78.0.13.ELlargesmp>" });
            outputs.Add("2.6");
            inputs.Add(new List<Object> { "Microsoft <R> Windows <R> 2000 Advanced Server 1.0" });
            outputs.Add("1.0");
            inputs.Add(new List<Object> { "Microsoft Windows XP Win2008R2 6.1.7601" });
            outputs.Add("6.1");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        
        //get-middle-name-from-full-name.sl
        //처음과 끝의 단어를 제외한 가운데의 복수의 단어들 출력
        [Test()]
        public void TestCase02()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "susan ann chang" });
            outputs.Add("ann");
            inputs.Add(new List<Object> { "ayako tanaka" });
            outputs.Add("");
            inputs.Add(new List<Object> { "bobby t. smith" });
            outputs.Add("t.");
            inputs.Add(new List<Object> { "anthory r. tom brown" });
            outputs.Add("r. tom");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow1.sl
        //
        [Test()]
        public void TestCase03()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Trucking Inc." });
            outputs.Add("Trucking");
            inputs.Add(new List<Object> { "New Truck Inc" });
            outputs.Add("New Truck");
            inputs.Add(new List<Object> { "ABV Trucking Inc, LLC" });
            outputs.Add("ABV Trucking");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //17212077.sl

        [Test()]
        public void TestCase04()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "01/15/2013" });
            outputs.Add("01/2013");
            inputs.Add(new List<Object> { "03/07/2011" });
            outputs.Add("03/2011");
            inputs.Add(new List<Object> { "05/09/2009" });
            outputs.Add("05/2009");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow10.sl

        [Test()]
        public void TestCase05()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "April 1 1799" });
            outputs.Add("1799");
            inputs.Add(new List<Object> { "April 11 1867" });
            outputs.Add("1867");
            inputs.Add(new List<Object> { "February 12 1806" });
            outputs.Add("1806");
            inputs.Add(new List<Object> { "February 21 1798" });
            outputs.Add("1798");
            inputs.Add(new List<Object> { "February 28 1844 as Delaware Township" });
            outputs.Add("1844");
            inputs.Add(new List<Object> { "February 5 1798" });
            outputs.Add("1798");
            inputs.Add(new List<Object> { "February 7 1892 Verona Township" });
            outputs.Add("1892");
            inputs.Add(new List<Object> { "February 9 1797" });
            outputs.Add("1797");
            inputs.Add(new List<Object> { "January 19 1748" });
            outputs.Add("1748");
            inputs.Add(new List<Object> { "July 10 1721 as Upper Penns Neck Township" });
            outputs.Add("1721");
            inputs.Add(new List<Object> { "March 15 1860" });
            outputs.Add("1860");
            inputs.Add(new List<Object> { "March 17 1870 <as Raritan Township>" });
            outputs.Add("1870");
            inputs.Add(new List<Object> { "March 17 1874" });
            outputs.Add("1874");
            inputs.Add(new List<Object> { "March 23 1864" });
            outputs.Add("1864");
            inputs.Add(new List<Object> { "March 5 1867" });
            outputs.Add("1867");
            inputs.Add(new List<Object> { "April 28th 1828" });
            outputs.Add("1828");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //25239569.sl
        
        [Test()]
        public void TestCase06()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Adf_ROCLeader_BAN_728x90_CPM_STD _BRD _NRT_DCK" });
            outputs.Add("Adf_ROCLeader_BAN_728x90_CPM_STD _Branding _NRT_DCK");
            inputs.Add(new List<Object> { "MMC_ContextualLarRec_BAN_336x280_CPM_STD _LDS _RTG_DCK" });
            outputs.Add("MMC_ContextualLarRec_BAN_336x280_CPM_STD _Leads _RTG_DCK");
            inputs.Add(new List<Object> { "Adf_ROC_DLBD_728x90_CPM_STD_DRS_NRT_NOR_DCK" });
            outputs.Add("Adf_ROC_DLBD_728x90_CPM_STD_Direct Response_NRT_NOR_DCK");


            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        
        //stackoverflow11.sl

        [Test()]
        public void TestCase07()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "2283-332-44543 CAP DDT PPL445_ HEEN PAX 77820" });
            outputs.Add("HEEN PAX 77820");
            inputs.Add(new List<Object> { "44372-33-3223 TYYTE CAP BOX 1550 244 BOX PPSSA223_ PAX" });
            outputs.Add("PAX");
            inputs.Add(new List<Object> { "PRECISE 77 CLEAR BLUE 99WIE_ BOX 4403 PAX SSKA" });
            outputs.Add("BOX 4403 PAX SSKA");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //31753108.sl

        [Test()]
        public void TestCase08()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Tire Pressure ABC123873 Monitor" });
            outputs.Add("ABC123873");
            inputs.Add(new List<Object> { " Oil Life ABC849999999021 gauge" });
            outputs.Add("ABC849999999021");
            inputs.Add(new List<Object> { " Air conditioner GHF211 maintenance" });
            outputs.Add("GHF211");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow2.sl

        [Test()]
        public void TestCase09()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();

            inputs.Add(new List<Object> { "india china japan" });
            outputs.Add("india china");
            inputs.Add(new List<Object> { "indonesia korea" });
            outputs.Add("indonesia");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //33619752.sl
        
        [Test()]
        public void TestCase10()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "General Electric" });
            outputs.Add("General Electric");
            inputs.Add(new List<Object> { "General Electric Inc" });
            outputs.Add("General Electric");
            inputs.Add(new List<Object> { "General Electric Company" });
            outputs.Add("General Electric");
            inputs.Add(new List<Object> { "Microsoft" });
            outputs.Add("Microsoft");
            inputs.Add(new List<Object> { "Microsoft Corporation" });
            outputs.Add("Microsoft");
            inputs.Add(new List<Object> { "Nintendo" });
            outputs.Add("Nintendo");
            inputs.Add(new List<Object> { "Nintendo Enterprises" });
            outputs.Add("Nintendo");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //initials_small.sl

        [Test()]
        public void TestCase11()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Nancy= FreeHafer" });
            outputs.Add("N.F.");
            inputs.Add(new List<Object> { "Andrew= Cencici" });
            outputs.Add("A.C.");
            inputs.Add(new List<Object> { "Jan= Kotas" });
            outputs.Add("J.K.");
            inputs.Add(new List<Object> { "Mariya= Sergienko" });
            outputs.Add("M.S.");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow3.sl
        
        [Test()]
        public void TestCase12()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();

            inputs.Add(new List<Object> { "geb. 14 oct 1956 Westerkerk HRL" });
            outputs.Add("Westerkerk HRL");
            inputs.Add(new List<Object> { "geb. 14 oct 1956 " });
            outputs.Add("");
            inputs.Add(new List<Object> { "geb. 15 feb 1987 Westerkerk HRL" });
            outputs.Add("Westerkerk HRL");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //35744094.sl
        
        [Test()]
        public void TestCase13()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "http=//www.apple.com/uk/mac" });
            outputs.Add("mac");
            inputs.Add(new List<Object> { "https=//www.microsoft.com/en-gb/windows" });
            outputs.Add("windows");
            inputs.Add(new List<Object> { "https=//www.microsoft.com/" });
            outputs.Add("microsoft");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow4.sl

        [Test()]
        public void TestCase14()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();

            inputs.Add(new List<Object> { "R/V<208,0,32>" });
            outputs.Add("R/V 208 0 32");
            inputs.Add(new List<Object> { "R/S<184,28,16>" });
            outputs.Add("R/S 184 28 16");
            inputs.Add(new List<Object> { "R/B<255,88,80>" });
            outputs.Add("R/B 255 88 80");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //36462127.sl

        [Test()]
        public void TestCase15()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "ABCDE/FGHI/JKL/MNOPQR" });
            outputs.Add("MNOPQR");
            inputs.Add(new List<Object> { "A/ABCDE/FGHI/JKL" });
            outputs.Add("JKL");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow5.sl

        [Test()]
        public void TestCase16()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "valentine day=1915=50==7.1=45" });
            outputs.Add("valentine day");
            inputs.Add(new List<Object> { "movie blah=2blahblah, The=1914=54==7.9=17" });
            outputs.Add("movie blah=2blahblah, The");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //38871714.sl
        
        [Test()]
        public void TestCase17()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "This is a <string>, It should be <changed> to <a> number." });
            outputs.Add("This is a string, It should be changed to a number.");
            inputs.Add(new List<Object> { "a < 4 and a > 0" });
            outputs.Add("a  4 and a  0");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow6.sl

        [Test()]
        public void TestCase18()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Dec 2, 2014, 11=23 PM - +91 90000 80000= loren ipsum" });
            outputs.Add("loren ipsum");
            inputs.Add(new List<Object> { "Dec 2, 2014, 11=24 PM - +91 90000 80000= loren" });
            outputs.Add("loren");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //39060015.sl
        
        [Test()]
        public void TestCase19()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "This is a line. /delete words in the area /keep this part" });
            outputs.Add("This is a line. keep this part");
            inputs.Add(new List<Object> { "hello /delete words in the area /obo" });
            outputs.Add("hello obo");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //phone-10_short.sl

        [Test()]
        public void TestCase20()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "+106 769-858-438" });
            outputs.Add("+106 (769) 858-438");
            inputs.Add(new List<Object> { "+83 973-757-831" });
            outputs.Add("+83 (973) 757-831");
            inputs.Add(new List<Object> { "+62 647-787-775" });
            outputs.Add("+62 (647) 787-775");
            inputs.Add(new List<Object> { "+172 027-507-632" });
            outputs.Add("+172 (027) 507-632");
            inputs.Add(new List<Object> { "+72 001-050-856" });
            outputs.Add("+72 (001) 050-856");
            inputs.Add(new List<Object> { "+95 310-537-401" });
            outputs.Add("+95 (310) 537-401");
            inputs.Add(new List<Object> { "+6 775-969-238" });
            outputs.Add("+6 (775) 969-238");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));

        }

        //stackoverflow7.sl
        
        [Test()]
        public void TestCase21()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Item 1 AQ-S810W-2AVDF", "AQ-S810W-2AVDF" });
            outputs.Add("Item 1 ");
            inputs.Add(new List<Object> { "Item 2 AQ-230A-1DUQ", "AQ-230A" });
            outputs.Add("Item 2 -1DUQ");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //43120683.sl
        
        [Test()]
        public void TestCase22()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "one, 1" });
            outputs.Add("one apple");
            inputs.Add(new List<Object> { "two, 2" });
            outputs.Add("two bananas");
            inputs.Add(new List<Object> { "three, 3" });
            outputs.Add("three strawberries");
            inputs.Add(new List<Object> { "four, 4" });
            outputs.Add("four oranges");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow8.sl

        [Test()]
        public void TestCase23()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "home/Excel/Sheet1.xls" });
            outputs.Add("Sheet1.xls");
            inputs.Add(new List<Object> { "home/user/Sheet1.xls" });
            outputs.Add("Sheet1.xls");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //43606446.sl

        [Test()]
        public void TestCase24()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "USD.EUR<IDEALPRO,CASH,EUR>" });
            outputs.Add("EUR");
            inputs.Add(new List<Object> { "USD.EUR<IDEALPRO,CASH,USD>" });
            outputs.Add("USD");
            inputs.Add(new List<Object> { "KOR.JPN<IDEALPRO,CASH,WON>" });
            outputs.Add("WON");
            inputs.Add(new List<Object> { "KOR.JPN<IDEALPRO,CASH,YEN>" });
            outputs.Add("YEN");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //stackoverflow9.sl

        [Test()]
        public void TestCase25()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Sarah Jane Jones" });
            outputs.Add("Jones");
            inputs.Add(new List<Object> { "Bob Jane Smithfield" });
            outputs.Add("Smithfield");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }



        //phone-5-short.sl

        [Test()]
        public void TestCase26()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "+106 769-858-438" });
            outputs.Add("106");
            inputs.Add(new List<Object> { "+83 973-757-831" });
            outputs.Add("83");
            inputs.Add(new List<Object> { "+62 647-787-775" });
            outputs.Add("62");
            inputs.Add(new List<Object> { "+172 027-507-632" });
            outputs.Add("172");
            inputs.Add(new List<Object> { "+72 001-050-856" });
            outputs.Add("72");
            inputs.Add(new List<Object> { "+95 310-537-401" });
            outputs.Add("95");
            inputs.Add(new List<Object> { "+6 775-969-238" });
            outputs.Add("6");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //strip-html-from-text-or-numbers.sl
        [Test()]
        public void TestCase27()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "<b>0.66<b>" });
            outputs.Add("0.66");
            inputs.Add(new List<Object> { "<b>0.409<b>" });
            outputs.Add("0.409");
            inputs.Add(new List<Object> { "<b>0.7268<b>" });
            outputs.Add("0.7268");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //clean-and-reformat-telephone-numbers.sl
        
        [Test()]
        public void TestCase28()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "801-456-8765" });
            outputs.Add("8014568765");
            inputs.Add(new List<Object> { "<978> 654-0299" });
            outputs.Add("9786540299");
            inputs.Add(new List<Object> { "978.654.0299" });
            outputs.Add("9786540299");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //strip-non-numeric-characters.sl

        [Test()]
        public void TestCase29()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "100 apples" });
            outputs.Add("100");
            inputs.Add(new List<Object> { "the price is %500 dollars" });
            outputs.Add("500");
            inputs.Add(new List<Object> { "serial number %003399" });
            outputs.Add("003399");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }



        //strip-numeric-characters-from-cell.sl
        
        [Test()]
        public void TestCase30()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "34653 jim mcdonald" });
            outputs.Add(" jim mcdonald");
            inputs.Add(new List<Object> { "price is 500" });
            outputs.Add("price is ");
            inputs.Add(new List<Object> { "100 apples" });
            outputs.Add(" apples");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //exceljet1.sl

        [Test()]
        public void TestCase31()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "ann chang", "achang_maaker.com" });
            outputs.Add("maaker.com");
            inputs.Add(new List<Object> { "bobby smith", "bobt_sphynx.uk.co" });
            outputs.Add("sphynx.uk.co");
            inputs.Add(new List<Object> { "art lennox", "art.lennox_svxn.com" });
            outputs.Add("svxn.com");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //exceljet2.sl

        [Test()]
        public void TestCase32()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "www.domain.com" });
            outputs.Add("com");
            inputs.Add(new List<Object> { "mail.net" });
            outputs.Add("net");
            inputs.Add(new List<Object> { "www.amaon.co.uk" });
            outputs.Add("uk");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //phone-6-short.sl

        [Test()]
        public void TestCase33()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "+106 769-858-438" });
            outputs.Add("769");
            inputs.Add(new List<Object> { "+83 973-757-831" });
            outputs.Add("973");
            inputs.Add(new List<Object> { "+62 647-787-775" });
            outputs.Add("647");
            inputs.Add(new List<Object> { "+172 027-507-632" });
            outputs.Add("027");
            inputs.Add(new List<Object> { "+72 001-050-856" });
            outputs.Add("001");
            inputs.Add(new List<Object> { "+95 310-537-401" });
            outputs.Add("310");
            inputs.Add(new List<Object> { "+6 775-969-238" });
            outputs.Add("775");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //exceljet3.sl
        
        //
        [Test()]
        public void TestCase34()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "year= 2016" });
            outputs.Add("2016");
            inputs.Add(new List<Object> { "make= subaru" });
            outputs.Add("subaru");
            inputs.Add(new List<Object> { "model= outback wagon" });
            outputs.Add("outback wagon");
            inputs.Add(new List<Object> { "fuel economy= 25/33" });
            outputs.Add("25/33");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //exceljet4.sl

        [Test()]
        public void TestCase35()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "https=//exceljet.net/catalog" });
            outputs.Add("exceljet.net/catalog");
            inputs.Add(new List<Object> { "https=//microsoft.com" });
            outputs.Add("microsoft.com");
            inputs.Add(new List<Object> { "ftp=//someserver.com" });
            outputs.Add("someserver.com");
            inputs.Add(new List<Object> { "sftp=//127.0.0.1" });
            outputs.Add("127.0.0.1");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //univ_4_short.sl
        
        [Test()]
        public void TestCase36()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "University of Pennsylvania", "Phialdelphia, PA, USA" });
            outputs.Add("Phialdelphia, PA, USA");
            inputs.Add(new List<Object> { "UCLA", "Los Angeles, CA" });
            outputs.Add("Los Angeles, CA, USA");
            inputs.Add(new List<Object> { "Cornell University", "Ithaca, New York, USA" });
            outputs.Add("Ithaca, NY, USA");
            inputs.Add(new List<Object> { "Penn", "Philadelphia, PA, USA" });
            outputs.Add("Philadelphia, PA, USA");
            inputs.Add(new List<Object> { "University of Maryland College Park", "College Park, MD" });
            outputs.Add("College Park, MD, USA");
            inputs.Add(new List<Object> { "University of Michigan", "Ann Arbor, MI, USA" });
            outputs.Add("Ann Arbor, MI, USA");
            inputs.Add(new List<Object> { "Columbia University", "New York, NY, USA" });
            outputs.Add("New York, NY, USA");
            inputs.Add(new List<Object> { "NYU", "New York, New York, USA" });
            outputs.Add("New York, NY, USA");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }


        //phone-7-short.sl

        [Test()]
        public void TestCase37()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "+106 769-858-438" });
            outputs.Add("858");
            inputs.Add(new List<Object> { "+83 973-757-831" });
            outputs.Add("757");
            inputs.Add(new List<Object> { "+62 647-787-775" });
            outputs.Add("787");
            inputs.Add(new List<Object> { "+172 027-507-632" });
            outputs.Add("507");
            inputs.Add(new List<Object> { "+72 001-050-856" });
            outputs.Add("050");
            inputs.Add(new List<Object> { "+95 310-537-401" });
            outputs.Add("537");
            inputs.Add(new List<Object> { "+6 775-969-238" });
            outputs.Add("969");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //extract-word-containing-specific-text.sl
        
        [Test()]
        public void TestCase38()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "send email to json_acme.com" });
            outputs.Add("json_acme.com");
            inputs.Add(new List<Object> { "contact help_robot.com for all support requests" });
            outputs.Add("help_robot.com");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //extract-word-that-begins-with-specific-character.sl
        
        [Test()]
        public void TestCase39()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "this is a _username in the middle" });
            outputs.Add("_username");
            inputs.Add(new List<Object> { "twitter names look like= _name " });
            outputs.Add("_name");
            inputs.Add(new List<Object> { "with two _name1 and _name2" });
            outputs.Add("_name1");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //univ_5_short.sl
        
        [Test()]
        public void TestCase40()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "University of Pennsylvania", "Phialdelphia, PA, USA" });
            outputs.Add("Phialdelphia, PA, USA");
            inputs.Add(new List<Object> { "UCLA", "Los Angeles, CA" });
            outputs.Add("Los Angeles, CA, USA");
            inputs.Add(new List<Object> { "Cornell University", "Ithaca, New York, USA" });
            outputs.Add("Ithaca, NY, USA");
            inputs.Add(new List<Object> { "Penn", "Philadelphia, PA, USA" });
            outputs.Add("Philadelphia, PA, USA");
            inputs.Add(new List<Object> { "University of Maryland College Park", "College Park, MD" });
            outputs.Add("College Park, MD, USA");
            inputs.Add(new List<Object> { "University of Michigan", "Ann Arbor, MI, USA" });
            outputs.Add("Ann Arbor, MI, USA");
            inputs.Add(new List<Object> { "Columbia University", "New York, NY, USA" });
            outputs.Add("New York, NY, USA");
            inputs.Add(new List<Object> { "NYU", "New York, New York, USA" });
            outputs.Add("New York, NY, USA");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //get-domain-name-from-url.sl
        //
        [Test()]
        public void TestCase41()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "https=//abc.com/def" });
            outputs.Add("https=//abc.com/");
            inputs.Add(new List<Object> { "http=//www.abc.com/def/cef" });
            outputs.Add("http=//www.abc.com/");
            inputs.Add(new List<Object> { "http=//chandoo.org/wp/def-def" });
            outputs.Add("http=//chandoo.org/");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //phone-9-short.sl

        [Test()]
        public void TestCase42()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "+106 769-858-438" });
            outputs.Add("106.769.858.438");
            inputs.Add(new List<Object> { "+83 973-757-831" });
            outputs.Add("83.973.757.831");
            inputs.Add(new List<Object> { "+62 647-787-775" });
            outputs.Add("62.647.787.775");
            inputs.Add(new List<Object> { "+172 027-507-632" });
            outputs.Add("172.027.507.632");
            inputs.Add(new List<Object> { "+72 001-050-856" });
            outputs.Add("72.001.050.856");
            inputs.Add(new List<Object> { "+95 310-537-401" });
            outputs.Add("95.310.537.401");
            inputs.Add(new List<Object> { "+6 775-969-238" });
            outputs.Add("6.775.969.238");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //get-last-line-in-cell.sl

        [Test()]
        public void TestCase43()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "11/1/2015 - First call/n12/3/2015-order placed/n11/15/2015-follow-up,interested" });
            outputs.Add("11/15/2015-follow-up,interested");
            inputs.Add(new List<Object> { "11/1/2015 - First call/n12/3/2015-order placed" });
            outputs.Add("12/3/2015-order placed");
            inputs.Add(new List<Object> { "11/1/2015 - First call" });
            outputs.Add("11/1/2015 - First call");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //remove-leading-and-trailing-spaces-from-text.sl

        [Test()]
        public void TestCase44()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "  The shawshank" });
            outputs.Add("The shawshank");
            inputs.Add(new List<Object> { "The    godfather" });
            outputs.Add("The godfather");
            inputs.Add(new List<Object> { "    pulp   fiction" });
            outputs.Add("pulp fiction");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //univ_6_short.sl
        
        [Test()]
        public void TestCase45()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "University of Pennsylvania", "Phialdelphia, PA, USA" });
            outputs.Add("Phialdelphia, PA, USA");
            inputs.Add(new List<Object> { "UCLA", "Los Angeles, CA" });
            outputs.Add("Los Angeles, CA, USA");
            inputs.Add(new List<Object> { "Cornell University", "Ithaca, New York, USA" });
            outputs.Add("Ithaca, NY, USA");
            inputs.Add(new List<Object> { "Penn", "Philadelphia, PA, USA" });
            outputs.Add("Philadelphia, PA, USA");
            inputs.Add(new List<Object> { "University of Maryland College Park", "College Park, MD" });
            outputs.Add("College Park, MD, USA");
            inputs.Add(new List<Object> { "University of Michigan", "Ann Arbor, MI, USA" });
            outputs.Add("Ann Arbor, MI, USA");
            inputs.Add(new List<Object> { "Columbia University", "New York, NY, USA" });
            outputs.Add("New York, NY, USA");
            inputs.Add(new List<Object> { "NYU", "New York, New York, USA" });
            outputs.Add("New York, NY, USA");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //get-last-name-from-name-with-comma.sl

        [Test()]
        public void TestCase46()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "chang,amy" });
            outputs.Add("amy,chang");
            inputs.Add(new List<Object> { "smith,bobby" });
            outputs.Add("bobby,smith");
            inputs.Add(new List<Object> { "lennox,aaron" });
            outputs.Add("aaron,lennox");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //remove-text-by-position.sl

        [Test()]
        public void TestCase47()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "c=/users/dave/shotcut.xls" });
            outputs.Add("shotcut.xls");
            inputs.Add(new List<Object> { "c=/users/dave/formulas.xls" });
            outputs.Add("formulas.xls");
            inputs.Add(new List<Object> { "c=/users/dave/pivot table.xls" });
            outputs.Add("pivot table.xls");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //get-last-word.sl

        [Test()]
        public void TestCase48()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "focus on one thing at a time" });
            outputs.Add("time");
            inputs.Add(new List<Object> { "premature opt is the root of all evil" });
            outputs.Add("evil");
            inputs.Add(new List<Object> { "where is life" });
            outputs.Add("life");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //11440431.sl
        [Test()]
        public void TestCase49()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Mining US" });
            outputs.Add("Mining");
            inputs.Add(new List<Object> { "Soybean Farming CAN" });
            outputs.Add("Soybean Farming");
            inputs.Add(new List<Object> { "Soybean Farming" });
            outputs.Add("Soybean Farming");
            inputs.Add(new List<Object> { "Oil Extraction US" });
            outputs.Add("Oil Extraction");
            inputs.Add(new List<Object> { "Fishing" });
            outputs.Add("Fishing");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }




        //name-combine-3_short.sl
        [Test()]
        public void TestCase50()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Launa", "Withers" });
            outputs.Add("L. Withers");
            inputs.Add(new List<Object> { "Lakenya", "Edison" });
            outputs.Add("L. Edison");
            inputs.Add(new List<Object> { "Brendan", "Hage" });
            outputs.Add("B. Hage");
            inputs.Add(new List<Object> { "Bradford", "Lango" });
            outputs.Add("B. Lango");
            inputs.Add(new List<Object> { "Rudolf", "Akiyama" });
            outputs.Add("R. Akiyama");
            inputs.Add(new List<Object> { "Lara", "Constable" });
            outputs.Add("L. Constable");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }


        //phone_short.sl
        [Test()]
        public void TestCase51()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "938-242-504" });
            outputs.Add("938");
            inputs.Add(new List<Object> { "308-916-545" });
            outputs.Add("308");
            inputs.Add(new List<Object> { "623-599-749" });
            outputs.Add("623");
            inputs.Add(new List<Object> { "981-424-843" });
            outputs.Add("981");
            inputs.Add(new List<Object> { "118-980-214" });
            outputs.Add("118");
            inputs.Add(new List<Object> { "244-655-094" });
            outputs.Add("244");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //2171308.sl
        [Test()]
        public void TestCase52()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "John Doe" });
            outputs.Add("J Doe");
            inputs.Add(new List<Object> { "Mayur Naik" });
            outputs.Add("M Naik");
            inputs.Add(new List<Object> { "Nimit Singh" });
            outputs.Add("N Singh");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }



        //create-email-address-from-name.sl
        [Test()]
        public void TestCase53()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "brown", "traci" });
            outputs.Add("tbrown_acme.com");
            inputs.Add(new List<Object> { "thomas", "linda" });
            outputs.Add("lthomas_acme.com");
            inputs.Add(new List<Object> { "ward", "jack" });
            outputs.Add("jward_acme.com");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }


        //28627624_1.sl
        [Test()]
        public void TestCase54()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Dec 2, 2014, 11=23 PM - +91 90000 80000= loren ipsum" });
            outputs.Add("Dec 2, 2014, 11=23 PM");
            inputs.Add(new List<Object> { "Dec 2, 2014, 11=24 PM - +91 90000 80000= loren" });
            outputs.Add("Dec 2, 2014, 11=24 PM");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //create-email-address-with-name-and-domain.sl
        [Test()]
        public void TestCase55()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "ayako", "ogawa", "acme.com" });
            outputs.Add("aogawa_acme.com");
            inputs.Add(new List<Object> { "amy", "johnson", "google.com" });
            outputs.Add("ajohnson_google.com");
            inputs.Add(new List<Object> { "tom", "chang", "upenn.edu" });
            outputs.Add("tchang_upenn.edu");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //name-combine-4_short.sl
        [Test()]
        public void TestCase56()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Launa", "Withers" });
            outputs.Add("Withers, L.");
            inputs.Add(new List<Object> { "Lakenya", "Edison" });
            outputs.Add("Edison, L.");
            inputs.Add(new List<Object> { "Brendan", "Hage" });
            outputs.Add("Hage, B.");
            inputs.Add(new List<Object> { "Bradford", "Lango" });
            outputs.Add("Lango, B.");
            inputs.Add(new List<Object> { "Rudolf", "Akiyama" });
            outputs.Add("Akiyama, R.");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //30732554.sl
        
        [Test()]
        public void TestCase57()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "TL-18273982| 10MM" });
            outputs.Add("TL-18273982");
            inputs.Add(new List<Object> { "TL-288762| 76DK" });
            outputs.Add("TL-288762");
            inputs.Add(new List<Object> { "CT-576" });
            outputs.Add("CT-576");
            inputs.Add(new List<Object> { "N/A" });
            outputs.Add("N/A");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //remove-file-extension-from-filename.sl
        [Test()]
        public void TestCase58()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "happy.jpg" });
            outputs.Add("happy");
            inputs.Add(new List<Object> { "pivot table.xls" });
            outputs.Add("pivot table");
            inputs.Add(new List<Object> { "sales data.csv" });
            outputs.Add("sales data");
            inputs.Add(new List<Object> { "invoice3001.xls.pdf" });
            outputs.Add("invoice3001");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //34801680.sl
        [Test()]
        public void TestCase59()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Name= ABC Retailers" });
            outputs.Add("ABC Retailers");
            inputs.Add(new List<Object> { " ame= XYZ Suppliers" });
            outputs.Add("XYZ Suppliers");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //remove-text-by-matching.sl
        [Test()]
        public void TestCase60()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "801-345-1987" });
            outputs.Add("8013451987");
            inputs.Add(new List<Object> { "612-554-2000" });
            outputs.Add("6125542000");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //35016216.sl
        
        [Test()]
        public void TestCase61()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "C0abc", "def" });
            outputs.Add("C0abc");
            inputs.Add(new List<Object> { "aabc", "def" });
            outputs.Add("def");
            inputs.Add(new List<Object> { "C0dd", "qwe" });
            outputs.Add("C0dd");
            inputs.Add(new List<Object> { "dd", "qwe" });
            outputs.Add("qwe");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //remove-unwanted-characters.sl
        
        [Test()]
        public void TestCase62()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "zx66448", "z" });
            outputs.Add("x66448");
            inputs.Add(new List<Object> { "zx66448", "x" });
            outputs.Add("z66448");
            inputs.Add(new List<Object> { "zx66448", "6" });
            outputs.Add("zx448");
            inputs.Add(new List<Object> { "zx66448", "4" });
            outputs.Add("zx668");
            inputs.Add(new List<Object> { "zx66448", "8" });
            outputs.Add("zx6644");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //dr-name_small.sl
        [Test()]
        public void TestCase63()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Nancy FreeHafer" });
            outputs.Add("Dr. Nancy");
            inputs.Add(new List<Object> { "Andrew Cencici" });
            outputs.Add("Dr. Andrew");
            inputs.Add(new List<Object> { "Jan Kotas" });
            outputs.Add("Dr. Jan");
            inputs.Add(new List<Object> { "Mariya Sergienko" });
            outputs.Add("Dr. Mariya");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //name-combine_short.sl
        [Test()]
        public void TestCase64()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Launa", "Withers" });
            outputs.Add("Launa Withers");
            inputs.Add(new List<Object> { "Lakenya", "Edison" });
            outputs.Add("Lakenya Edison");
            inputs.Add(new List<Object> { "Brendan", "Hage" });
            outputs.Add("Brendan Hage");
            inputs.Add(new List<Object> { "Bradford", "Lango" });
            outputs.Add("Bradford Lango");
            inputs.Add(new List<Object> { "Rudolf", "Akiyama" });
            outputs.Add("Rudolf Akiyama");
            inputs.Add(new List<Object> { "Lara", "Constable" });
            outputs.Add("Lara Constable");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //replace-one-character-with-another.sl
        [Test()]
        public void TestCase65()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "801 456 8756" });
            outputs.Add("801-456-8756");
            inputs.Add(new List<Object> { "978 456 8756" });
            outputs.Add("978-456-8756");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //37534494.sl
        
        [Test()]
        public void TestCase66()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "I love apples", "I hate bananas", "banana" });
            outputs.Add("I hate bananas");
            inputs.Add(new List<Object> { "I love apples", "I hate bananas", "apple" });
            outputs.Add("I love apples");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //38664547.sl
        
        [Test()]
        public void TestCase67()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "thatensures" });
            outputs.Add("ensures");
            inputs.Add(new List<Object> { "thatwill" });
            outputs.Add("will");
            inputs.Add(new List<Object> { "thathave" });
            outputs.Add("have");
            inputs.Add(new List<Object> { "knowthat" });
            outputs.Add("know");
            //inputs.Add(new List<Object> { "that" });
            //outputs.Add("");
            //inputs.Add(new List<Object> { "mouse" });
            //outputs.Add("mouse");
            inputs.Add(new List<Object> { "knowthat" });
            outputs.Add("know");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        ////41503046.sl
        [Test()]
        public void TestCase68()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Polygonum amphibium" });
            outputs.Add("Polygonum");
            inputs.Add(new List<Object> { "Hippuris vulgaris" });
            outputs.Add("Hippuris");
            inputs.Add(new List<Object> { "Lysimachia vulgaris" });
            outputs.Add("Lysimachia");
            inputs.Add(new List<Object> { "Juncus bulbosus ssp. bulbosus" });
            outputs.Add("Juncus bulbosus");
            inputs.Add(new List<Object> { "Lycopus europaeus ssp. europaeus" });
            outputs.Add("Lycopus europaeus");
            inputs.Add(new List<Object> { "Nymphaea alba" });
            outputs.Add("Nymphaea");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //firstname_small.sl
        [Test()]
        public void TestCase69()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Nancy FreeHafer" });
            outputs.Add("Nancy");
            inputs.Add(new List<Object> { "Andrew Cencici" });
            outputs.Add("Andrew");
            inputs.Add(new List<Object> { "Jan Kotas" });
            outputs.Add("Jan");
            inputs.Add(new List<Object> { "Mariya Sergienko" });
            outputs.Add("Mariya");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //phone-1_short.sl
        [Test()]
        public void TestCase70()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "938-242-504" });
            outputs.Add("242");
            inputs.Add(new List<Object> { "308-916-545" });
            outputs.Add("916");
            inputs.Add(new List<Object> { "623-599-749" });
            outputs.Add("599");
            inputs.Add(new List<Object> { "981-424-843" });
            outputs.Add("424");
            inputs.Add(new List<Object> { "118-980-214" });
            outputs.Add("980");
            inputs.Add(new List<Object> { "244-655-094" });
            outputs.Add("655");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //reverse-name_short.sl
        [Test()]
        public void TestCase71()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Launa", "Withers" });
            outputs.Add("Withers Launa");
            inputs.Add(new List<Object> { "Lakenya", "Edison" });
            outputs.Add("Edison Lakenya");
            inputs.Add(new List<Object> { "Brendan", "Hage" });
            outputs.Add("Hage Brendan");
            inputs.Add(new List<Object> { "Bradford", "Lango" });
            outputs.Add("Lango Bradford");
            inputs.Add(new List<Object> { "Rudolf", "Akiyama" });
            outputs.Add("Akiyama Rudolf");
            inputs.Add(new List<Object> { "Lara", "Constable" });
            outputs.Add("Constable Lara");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //add-a-line-break-with-a-formula.sl
        [Test()]
        public void TestCase72()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Traci Brown", "1301 Robinson Court", "Saginaw, MI 48607" });
            outputs.Add("Traci Brown/n1301 Robinson Court/nSaginaw, MI 48607");
            inputs.Add(new List<Object> { "Mary Hannan", "1195 Amethyst Drive", "Lansing, MI 48933" });
            outputs.Add("Mary Hannan/n1195 Amethyst Drive/nLansing, MI 48933");
            inputs.Add(new List<Object> { "Linda Thomas", "2479 North Bend Road", "Allen, KY 41601" });
            outputs.Add("Linda Thomas/n2479 North Bend Road/nAllen, KY 41601");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //get-domain-from-email-address_2.sl
        [Test()]
        public void TestCase73()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "ann chang", "achang_maaker.com" });
            outputs.Add("achang");
            inputs.Add(new List<Object> { "bobby smith", "bobt_sphynx.uk.co" });
            outputs.Add("bobt");
            inputs.Add(new List<Object> { "art lennox", "art.lennox_svxn.com" });
            outputs.Add("art.lennox");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        ////get-first-name-from-name.sl
        [Test()]
        public void TestCase74()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Susan Ann Chang" });
            outputs.Add("Susan");
            inputs.Add(new List<Object> { "Ayako Tanaka" });
            outputs.Add("Ayako");
            inputs.Add(new List<Object> { "Bobby T. smth" });
            outputs.Add("Bobby");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //get-first-word.sl
        
        [Test()]
        public void TestCase75()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "The quick brown fox." });
            outputs.Add("The");
            inputs.Add(new List<Object> { "quick brown fox." });
            outputs.Add("quick");
            inputs.Add(new List<Object> { "fox" });
            outputs.Add("");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        ////get-last-name-from-name.sl
        [Test()]
        public void TestCase76()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Park Kim" });
            outputs.Add("Kim");
            inputs.Add(new List<Object> { "Lee Kim" });
            outputs.Add("Kim");
            inputs.Add(new List<Object> { "Kim Lee" });
            outputs.Add("Lee");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        ////phone-2_short.sl
        [Test()]
        public void TestCase77()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "938-242-504" });
            outputs.Add("504");
            inputs.Add(new List<Object> { "308-916-545" });
            outputs.Add("545");
            inputs.Add(new List<Object> { "623-599-749" });
            outputs.Add("749");
            inputs.Add(new List<Object> { "981-424-843" });
            outputs.Add("843");
            inputs.Add(new List<Object> { "118-980-214" });
            outputs.Add("214");
            inputs.Add(new List<Object> { "244-655-094" });
            outputs.Add("094");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        ////bikes_small.sl
        [Test()]
        public void TestCase78()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Ducati100" });
            outputs.Add("Ducati");
            inputs.Add(new List<Object> { "Honda125" });
            outputs.Add("Honda");
            inputs.Add(new List<Object> { "Ducati250" });
            outputs.Add("Ducati");
            inputs.Add(new List<Object> { "Honda250" });
            outputs.Add("Honda");
            inputs.Add(new List<Object> { "Honda550" });
            outputs.Add("Honda");
            inputs.Add(new List<Object> { "Ducati125" });
            outputs.Add("Ducati");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //join-cells-with-comma.sl
        
        [Test()]
        public void TestCase79()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "figs", "", "apples" });
            outputs.Add("figs, apples");
            inputs.Add(new List<Object> { "mangos", "kiwis", "grapes" });
            outputs.Add("mangos, kiwis, grapes");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //join-first-and-last-name.sl
        [Test()]
        public void TestCase80()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "susan", "chang" });
            outputs.Add("susan chang");
            inputs.Add(new List<Object> { "aaron", "kim" });
            outputs.Add("aaron kim");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //univ_1_short.sl
        [Test()]
        public void TestCase81()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "University of Pennsylvania", "Phialdelphia, PA, USA" });
            outputs.Add("University of Pennsylvania, Phialdelphia, PA, USA");
            inputs.Add(new List<Object> { "Cornell University", "Ithaca, New York, USA" });
            outputs.Add("Cornell University, Ithaca, New York, USA");
            inputs.Add(new List<Object> { "University of Maryland College Park", "College Park, MD" });
            outputs.Add("University of Maryland College Park, College Park, MD");
            inputs.Add(new List<Object> { "University of Michigan", "Ann Arbor, MI, USA" });
            outputs.Add("University of Michigan, Ann Arbor, MI, USA");
            inputs.Add(new List<Object> { "Yale University", "New Haven, CT, USA" });
            outputs.Add("Yale University, New Haven, CT, USA");
            inputs.Add(new List<Object> { "Columbia University", "New York, NY, USA" });
            outputs.Add("Columbia University, New York, NY, USA");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }


        //lastname.sl
        [Test()]
        public void TestCase82()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Nancy FreeHafer" });
            outputs.Add("FreeHafer");
            inputs.Add(new List<Object> { "Andrew Cencici" });
            outputs.Add("Cencici");
            inputs.Add(new List<Object> { "Jan Kotas" });
            outputs.Add("Kotas");
            inputs.Add(new List<Object> { "Mariya Sergienko" });
            outputs.Add("Sergienko");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //lastname_small.sl
        [Test()]
        public void TestCase83()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Nancy FreeHafer" });
            outputs.Add("FreeHafer");
            inputs.Add(new List<Object> { "Andrew Cencici" });
            outputs.Add("Cencici");
            inputs.Add(new List<Object> { "Jan Kotas" });
            outputs.Add("Kotas");
            inputs.Add(new List<Object> { "Mariya Sergienko" });
            outputs.Add("Sergienko");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //most-frequently-occurring-text.sl
        
        [Test()]
        public void TestCase84()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "cat", "dog", "cat" });
            outputs.Add("cat");
            inputs.Add(new List<Object> { "blue", "red", "red" });
            outputs.Add("red");
            inputs.Add(new List<Object> { "firm", "firm", "soft" });
            outputs.Add("firm");
            inputs.Add(new List<Object> { "soft", "soft", "soft" });
            outputs.Add("soft");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //univ_2_short.sl
        
        [Test()]
        public void TestCase85()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "University of Pennsylvania", "Phialdelphia, PA, USA" });
            outputs.Add("University of Pennsylvania, Phialdelphia, PA, USA");
            inputs.Add(new List<Object> { "UCLA", "Los Angeles, CA" });
            outputs.Add("UCLA, Los Angeles, CA, USA");
            inputs.Add(new List<Object> { "Cornell University", "Ithaca, New York, USA" });
            outputs.Add("Cornell University, Ithaca, New York, USA");
            inputs.Add(new List<Object> { "Penn", "Philadelphia, PA, USA" });
            outputs.Add("Penn, Philadelphia, PA, USA");
            inputs.Add(new List<Object> { "University of Maryland College Park", "College Park, MD" });
            outputs.Add("University of Maryland College Park, College Park, MD, USA");
            inputs.Add(new List<Object> { "University of Michigan", "Ann Arbor, MI, USA" });
            outputs.Add("University of Michigan, Ann Arbor, MI, USA");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //phone-4_short.sl
        [Test()]
        public void TestCase86()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "938-242-504" });
            outputs.Add("938.242.504");
            inputs.Add(new List<Object> { "308-916-545" });
            outputs.Add("308.916.545");
            inputs.Add(new List<Object> { "623-599-749" });
            outputs.Add("623.599.749");
            inputs.Add(new List<Object> { "981-424-843" });
            outputs.Add("981.424.843");
            inputs.Add(new List<Object> { "118-980-214" });
            outputs.Add("118.980.214");
            inputs.Add(new List<Object> { "244-655-094" });
            outputs.Add("244.655.094");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //change-negative-numbers-to-positive.sl
        
        [Test()]
        public void TestCase87()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "-%134" });
            outputs.Add("%134");
            inputs.Add(new List<Object> { "500" });
            outputs.Add("500");
            inputs.Add(new List<Object> { "5.125" });
            outputs.Add("5.125");
            inputs.Add(new List<Object> { "-%43.00" });
            outputs.Add("%43.00");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //name-combine-2_short.sl
        [Test()]
        public void TestCase88()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "Nancy", "FreeHafer" });
            outputs.Add("Nancy F.");
            inputs.Add(new List<Object> { "Andrew", "Cencici" });
            outputs.Add("Andrew C.");
            inputs.Add(new List<Object> { "Jan", "Kotas" });
            outputs.Add("Jan K.");
            inputs.Add(new List<Object> { "Mariya", "Sergienko" });
            outputs.Add("Mariya S.");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }
        //univ_3_short.sl
        
        [Test()]
        public void TestCase89()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "University of Pennsylvania", "Phialdelphia, PA, USA" });
            outputs.Add("Phialdelphia, PA, USA");
            inputs.Add(new List<Object> { "UCLA", "Los Angeles, CA" });
            outputs.Add("Los Angeles, CA, USA");
            inputs.Add(new List<Object> { "Cornell University", "Ithaca, New York, USA" });
            outputs.Add("Ithaca, New York, USA");
            inputs.Add(new List<Object> { "Penn", "Philadelphia, PA, USA" });
            outputs.Add("Philadelphia, PA, USA");
            inputs.Add(new List<Object> { "University of Maryland College Park", "College Park, MD" });
            outputs.Add("College Park, MD, USA");
            inputs.Add(new List<Object> { "University of Michigan", "Ann Arbor, MI, USA" });
            outputs.Add("Ann Arbor, MI, USA");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

        //phone-8_short.sl
        [Test()]
        public void TestCase90()
        {
            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            inputs.Add(new List<Object> { "+106 769-858-438" });
            outputs.Add("438");
            inputs.Add(new List<Object> { "+83 973-757-831" });
            outputs.Add("831");
            inputs.Add(new List<Object> { "+62 647-787-775" });
            outputs.Add("775");
            inputs.Add(new List<Object> { "+172 027-507-632" });
            outputs.Add("632");
            inputs.Add(new List<Object> { "+72 001-050-856" });
            outputs.Add("856");
            inputs.Add(new List<Object> { "+95 310-537-401" });
            outputs.Add("401");
            inputs.Add(new List<Object> { "+6 775-969-238" });
            outputs.Add("238");
            Assert.AreEqual(true, Program.SynthesisFunc(inputs, outputs));
        }

    }
}
