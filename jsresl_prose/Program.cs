using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ProgramSynthesis;
using Microsoft.ProgramSynthesis.AST;
using Microsoft.ProgramSynthesis.Compiler;
using Microsoft.ProgramSynthesis.Learning;
using Microsoft.ProgramSynthesis.Learning.Logging;
using Microsoft.ProgramSynthesis.Learning.Strategies;
using Microsoft.ProgramSynthesis.Specifications;
using jsresl_prose.Learning;
using jsresl_prose.Semantics;
using Microsoft.ProgramSynthesis.VersionSpace;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;

namespace jsresl_prose
{
    class Program
    {
        public static ProgramNode bestProgram = null;
        public static int BestDone = -1;
        public static void WriteColored(ConsoleColor color, object obj)
            => WriteColored(color, () => Console.WriteLine(obj));

        public static void WriteColored(ConsoleColor color, Action write)
        {
            ConsoleColor currentColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            write();
            Console.ForegroundColor = currentColor;
        }

        public static bool SynthesisFunc(List<List<Object>> inputs, List<Object> outputs)
        {
            var grammar_raw = DSLCompiler.ParseGrammarFromFile("jsresl_prose.grammar");
            var grammar = grammar_raw.Value;
            if (grammar == null)
            {
                WriteColored(ConsoleColor.Magenta, grammar_raw.TraceDiagnostics);
                return false;
            }
            var examples_list = new Dictionary<State, IEnumerable<object>>();
            var n_examples = inputs.Count;
            for (int i = 0; i < n_examples; i++)
            {
                var inputState = State.Create(grammar.Symbol("vs"), inputs[i].ToArray());
                var output_spec = new HashSet<Object>();
                output_spec.Add(outputs[i]);
                examples_list[inputState] = output_spec.Cast<object>();
            }

            var spec = new DisjunctiveExamplesSpec(examples_list);
            var engine = new SynthesisEngine(grammar, new SynthesisEngine.Config
            {
                Strategies = new ISynthesisStrategy[]
                {
                    new EnumerativeSynthesis(),
                    new DeductiveSynthesis(new WitnessFunctions(grammar)),
                },
                UseThreads = false,
                LogListener = new LogListener(),
                UseDynamicSoundnessCheck = true,
                SynthesisTimeout = new TimeSpan(0, 0, 2)
            });
            var scoreFeature = new RankingScore(grammar);

            ProgramSet consistentPrograms = engine.LearnGrammar(spec);
            Console.WriteLine($"|Number of all programs|: {consistentPrograms.Size}");
            var program = new Program();
            program.Test(consistentPrograms, scoreFeature).Wait();
            while (BestDone < 0) { }
            if (BestDone == 0)
            {
                WriteColored(ConsoleColor.Red, "No program :(");
                return false;
            }
            else if (BestDone == 1)
            {
                var score = bestProgram.GetFeatureValue(scoreFeature);
                WriteColored(ConsoleColor.Cyan, $"[score = {score:F3}] {bestProgram}");

            }
            else if (BestDone == 2)
            {
                if (consistentPrograms.Volume > 1)
                {
                    WriteColored(ConsoleColor.Red, "Ranking takes too long. Just randomly picking up a program...");
                    bestProgram = consistentPrograms.Sample(new Random());
                    WriteColored(ConsoleColor.Cyan, $"[score = N/A] {bestProgram}");
                }
            }

            WriteColored(ConsoleColor.DarkGreen, "======= Javascript program ======= ");
            WriteColored(ConsoleColor.DarkGreen, $"function test(vs){{ return {ASTTransformer.toJS(bestProgram)} }} ");

            return true;
        }
        public static void Main(string[] args)
        {
            var grammar_raw = DSLCompiler.ParseGrammarFromFile("jsresl_prose.grammar");
            var grammar = grammar_raw.Value;
            if (grammar == null)
            {
                WriteColored(ConsoleColor.Magenta, grammar_raw.TraceDiagnostics);
                return;
            }

            var inputs = new List<List<Object>>();
            var outputs = new List<Object>();
            var examples_list = new Dictionary<State, IEnumerable<object>>();
            var n_examples = inputs.Count;
            var is_input = true;
            Console.Out.WriteLine("Provide a new input-output examples (If You have done, press [control] + [D]");
            if (inputs.Count == 0)
            {
                try
                {
                    Console.Out.Write("inputs (e.g. \"Lee\" \"woosuk\" ): ");
                    string input;
                    while ((input = Console.ReadLine()) != null)
                    {
                        //string[] numbers = Console.ReadLine().Split('_');
                        var strings_line = new List<Object>();
                        int startExample = 0;
                        int endExample = 0;
                        if (is_input && input != "")
                        {
                            while (startExample < input.Length)
                            {
                                startExample = input.IndexOf("\"", startExample, StringComparison.Ordinal) + 1;
                                endExample = input.IndexOf("\"", startExample + 1, StringComparison.Ordinal) + 1;
                                //Console.WriteLine($"{startExample} {endExample} {strings_line.Count}");
                                if (startExample == -1 || endExample == -1) break;
                                strings_line.Add(input.Substring(startExample, endExample - startExample - 1));
                                startExample = endExample + 1;
                            }
                            inputs.Add(strings_line);
                            is_input = false;
                        }

                        else if (!is_input && input != "")
                        {
                            var output = input.Replace("\"", "");
                            if (output != null) outputs.Add(output);

                            is_input = true;
                        }
                        if (is_input) Console.Out.Write("inputs (e.g. \"Lee\" \"woosuk\" ): ");
                        else Console.Out.Write("output (e.g. \"Lee woosuk\" ): ");
                        n_examples++;

                    }
                }
                catch (Exception)
                {
                    throw new Exception("Invalid example format. Please try again. input and out should be between quotes");
                }
            }
            SynthesisFunc(inputs, outputs);

        }


        public async Task Test(ProgramSet consistentPrograms, RankingScore scoreFeature)
        {
            await DoWithTimeout(() =>
            {
                bestProgram = consistentPrograms.TopK(scoreFeature).FirstOrDefault();
                if (bestProgram == null) BestDone = 0;
                else BestDone = 1;
            },
            () => Console.WriteLine(""),
            TimeSpan.FromSeconds(3));
        }

        public async Task DoWithTimeout(Action action, Action timeoutAction, TimeSpan timeout)
        {
            var cancellatinSource = new CancellationTokenSource();

            var task = Task.Run(action, cancellatinSource.Token);

            if (await Task.WhenAny(task, Task.Delay(timeout)) != task)
            {
                cancellatinSource.Cancel();
                timeoutAction();
                BestDone = 2;
            }

        }
    }
}
