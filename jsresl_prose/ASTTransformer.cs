﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ProgramSynthesis;
using Microsoft.ProgramSynthesis.AST;
using Microsoft.ProgramSynthesis.Compiler;
using Microsoft.ProgramSynthesis.Learning;
using Microsoft.ProgramSynthesis.Learning.Logging;
using Microsoft.ProgramSynthesis.Learning.Strategies;
using Microsoft.ProgramSynthesis.Specifications;
using jsresl_prose.Learning;
using jsresl_prose.Semantics;
using Microsoft.ProgramSynthesis.VersionSpace;
using System.IO;
using System.Diagnostics;

namespace jsresl_prose
{
    class ASTNodeVisitor : ProgramNodeVisitor<string>
    {

        private static Stack<string> domainVar = new Stack<string>();
        private static Boolean domainLet = false;
        private static String concatLeft = "";
        private static String concatRight = "";
        private static int depth = 0;

        private string changeVar(string input)
        {
            if (input == "vs")
            {
                return "vs";
            }
            else if (input == "x")
            {
                return domainVar.Peek();
            }
            else
            {
                return "";
            }
        }

        private string indent()
        {
            return new String(' ', depth);
        }
        public override string VisitNonterminal(NonterminalNode node)
        {
            var children = node.Children;
            string rule_id = node.Rule.Id;
            string result = "";

            var children_strings = new List<string>();
            foreach (var child in children)
            {
                children_strings.Add(child.AcceptVisitor(new ASTNodeVisitor()));
            }

            // Console.WriteLine(rule_id);
            if (rule_id == "ToUpperCase")
            {
                result += string.Concat(children_strings.ToArray());
                result += ".toUpperCase()";

            }
            else if (rule_id == "ToLowerCase")
            {
                result += string.Concat(children_strings.ToArray());
                result += ".toLowerCase()";
            }
            else if (rule_id == "Concat")
            {
                // result += "\'\'";
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                //     result += $".concat({child_str})";
                // }
                result += concatLeft + "+" + concatRight;
            }
            else if (rule_id == "Nth_str")
            {
                // Console.WriteLine("Nth_str----------(vs,k)");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                if (domainLet)
                {
                    domainVar.Push(children_strings[0] + "[" + children_strings[1] + "]");
                    domainLet = false;
                }
                else
                {
                    result += children_strings[0] + "[" + children_strings[1] + "]";
                }
                // result += "Nth_str";
            }
            else if (rule_id == "RegexPair")
            {
                // Console.WriteLine("RegexPair----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                var RegexString = "";
                var left = children_strings[0].Substring(1, children_strings[0].Length - 2);
                var right = children_strings[1].Substring(1, children_strings[1].Length - 2);
                RegexString += "(" + left + ")";
                RegexString += "(" + right + ")";
                RegexString =
                    RegexString
                    .Replace("/", "\\/")
                    .Replace("\\\\", "\\")
                    .Replace("\\,", ",");
                //fix dependency of javascript 
                RegexString = "/" + RegexString + "/g";
                // make regex
                if (left.Contains("\\p{") || right.Contains("\\p{"))
                    RegexString += "u";

                result += RegexString;
            }
            else if (rule_id == "RegexPosition")
            {
                // Console.WriteLine("RegexPosition----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                // result += "RegexPosition";x
                result += "Array.from(" + changeVar(children_strings[0]) + ".matchAll(" + children_strings[1] + "),_tmp=>_tmp.index+_tmp[1].length).slice(" + children_strings[2] + ")[0]";
            }
            else if (rule_id == "AbsolutePosition")
            {
                // Console.WriteLine("AbsolutePosition----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                // result += "AbsolutePosition";

                result += children_strings[1];
            }
            else if (rule_id == "PositionPair")
            {
                // Console.WriteLine("PositionPair----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                // result += "PositionPair1";
                int x, y;
                Boolean xflag = false, yflag = false;
                if (Int32.TryParse(children_strings[0], out x))
                {
                    xflag = true;
                    if (x < 0) x++;
                }
                if (Int32.TryParse(children_strings[1], out y))
                {
                    yflag = true;
                    if (y < 0) y++;
                }
                result += (xflag ? x.ToString() : children_strings[0]);

                if (yflag)
                {
                    if (y != 0)
                        result += "," + y.ToString();
                }
                else
                {
                    result += "," + children_strings[1];
                }
            }
            else if (rule_id == "Substring")
            {
                // Console.WriteLine("Substring----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                // result += "Substring";
                result += changeVar(children_strings[0]) + ".slice(" + children_strings[1] + ")";
            }
            else if (rule_id == "StrPair")
            {
                // Console.WriteLine("StrPair----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                result += children_strings[0] + "," + children_strings[1];
                concatLeft = children_strings[0];
                concatRight = children_strings[1];
            }
            else if (rule_id == "Replace")
            {
                // Console.WriteLine("Replace----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                result += changeVar(children_strings[0]) + ".replace(" + children_strings[1] + ")";
            }
            else if (rule_id == "Delete")
            {
                // Console.WriteLine("Replace----------");
                // foreach (var child_str in children_strings)
                // {
                //     Console.WriteLine(child_str);
                // }
                result += changeVar(children_strings[0]) + ".replace(" + children_strings[1] + ", \"\" )";
            }
            else
            {
                result = string.Concat(children_strings.ToArray());
            }
            return result;
        }

        public override string VisitLet(LetNode node)
        {
            // Console.WriteLine(node);
            // let x = child_0 in child_1 
            //Console.WriteLine($"let : {node.Children[0].ToString()}");
            //Console.WriteLine($"let : {node.Children[1].ToString()}");
            var result = "";
            // result += "[start] ";
            domainLet = true;
            result += node.Children[0].AcceptVisitor(new ASTNodeVisitor());
            result += node.Children[1].AcceptVisitor(new ASTNodeVisitor());
            domainVar.Pop();
            // result += "[end]";
            return result;
        }

        public override string VisitLiteral(LiteralNode node)
        {
            // Console.WriteLine(node);
            return node.ToString();
        }

        public override string VisitVariable(VariableNode node)
        {
            // Console.WriteLine(node);
            return node.ToString();
        }

        public override string VisitHole(Hole node)
        {
            throw new NotImplementedException();
        }

        public override string VisitLambda(LambdaNode node)
        {
            throw new NotImplementedException();
        }

    }


    public class ASTTransformer
    {
        public static string toJS(ProgramNode program)
        {
            return program.AcceptVisitor(new ASTNodeVisitor());
        }
    }
}
