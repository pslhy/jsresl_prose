using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using Microsoft.ProgramSynthesis;
using Microsoft.ProgramSynthesis.AST;
using Microsoft.ProgramSynthesis.Features;

namespace jsresl_prose.Learning
{
    public class RankingScore : Feature<double>
    {
        public RankingScore(Grammar grammar) : base(grammar, "Score") { }

        protected override double GetFeatureValueForVariable(VariableNode variable) => 0;

        [FeatureCalculator("Nth_str", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreNth_str(double vsScore, double kScore) => vsScore + 1;

        [FeatureCalculator("ConstStr", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreConStr(double sScore) => sScore + 1;

        [FeatureCalculator("ToUpperCase", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreToUpperCase(double sScore) => Math.Atan(sScore);

        [FeatureCalculator("ToLowerCase", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreToLowerCase(double sScore) => Math.Atan(sScore);

        [FeatureCalculator("Delete", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreDelete(double inScore, double ssScore) => Math.Atan((ssScore + inScore) / 5);

        [FeatureCalculator("Concat", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreConcat(double ssScore) => Math.Atan(ssScore);

        [FeatureCalculator("StrPair", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreStrPair(double s1Score, double s2Score) => Math.Atan(0.1 * (s1Score + s2Score));

        [FeatureCalculator("Substring", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreSubstring(double inScore, double rrScore) => Math.Atan(rrScore);

        [FeatureCalculator("PositionPair", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScorePositionPair(double inScore, double rrScore) => Math.Atan(rrScore);

        [FeatureCalculator("RegexPair", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreRegexPair(double inScore, double rrScore) => Math.Atan(rrScore);

        [FeatureCalculator("r", Method = CalculationMethod.FromLiteral)]
        double RScore(Regex r) => r.ToString().Length == 0 ? 0 : Math.Atan(1 / (r.ToString().Length + 100000));

        [FeatureCalculator("RegexPosition", Method = CalculationMethod.FromChildrenFeatureValues)]
        double ScoreRegexPosition(double inScore, double rrScore, double kScore) => Math.Atan(rrScore);

        [FeatureCalculator("AbsolutePosition", Method = CalculationMethod.FromChildrenNodes)]
        double ScoreAbsolutePosition(VariableNode x, LiteralNode k)
        {
            // double score = x.GetFeatureValue(this) + k.GetFeatureValue(this);
            return Math.Atan(1 / (x.GetFeatureValue(this) + 100));
        }

        [FeatureCalculator("s", Method = CalculationMethod.FromLiteral)]
        double SScore(string s) => s.Length;

        [FeatureCalculator("k", Method = CalculationMethod.FromLiteral)]
        double KScore(int k) => Math.Atan(1 / (Math.Abs(k) + 100));
    }
}
