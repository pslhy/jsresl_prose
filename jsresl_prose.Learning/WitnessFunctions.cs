﻿using Microsoft.ProgramSynthesis;
using Microsoft.ProgramSynthesis.Specifications;
using Microsoft.ProgramSynthesis.Learning;
using System.Collections.Generic;
using System;
using Microsoft.ProgramSynthesis.Rules;
using System.Linq;
using System.Text.RegularExpressions;


namespace jsresl_prose.Learning
{
    public class WitnessFunctions : DomainLearningLogic
    {
        private static Dictionary<Tuple<State, string, int, Object>, HashSet<object>> cache;
        private static Dictionary<Tuple<State, string, int, Object, Object>, int> cache2;
        private int threshold = 20;

        private bool AlreadyHandled(State inputState, string operName, int paramIndex, Object output, Object value)
        {
            var index = new Tuple<State, string, int, Object, Object>(inputState, operName, paramIndex, output, value);
            if (!cache2.ContainsKey(index))
            {
                cache2[index] = 0;
            }
            cache2[index]++;
            if (cache2[index] > threshold)
            {
                return true;
            }
            return false;

        }

        private void AddIfNew(State inputState, string operName, int paramIndex, Object output, string value, ref HashSet<string> values)
        {
            var index = new Tuple<State, string, int, Object>(inputState, operName, paramIndex, output);

            if (!cache.ContainsKey(index))
            {
                cache[index] = new HashSet<object>();
            }
            if (!cache[index].Contains(value))
            {
                cache[index].Add(value);
                values.Add(value);
            }
        }

        public WitnessFunctions(Grammar grammar) : base(grammar)
        {
            cache = new Dictionary<Tuple<State, string, int, Object>, HashSet<object>>();
            cache2 = new Dictionary<Tuple<State, string, int, Object, Object>, int>();
        }

        [WitnessFunction("Nth_str", 1)]
        internal DisjunctiveExamplesSpec WitnessNth_str(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var vs = (Object[])inputState[rule.Body[0]];

                var occurrences = new HashSet<int>();
                foreach (var val in example.Value)
                {
                    var str_value = (string)val;
                    for (int i = 0; i < vs.Length; i++)
                    {
                        if (vs[i] is string && ((string)vs[i]).Equals(str_value))
                        {
                            occurrences.Add(i);
                        }
                    }
                }
                if (occurrences.Count == 0) return null;
                result[inputState] = occurrences.Cast<object>();
            }
            return new DisjunctiveExamplesSpec(result);
        }

        [WitnessFunction("ConstStr", 0)]
        internal DisjunctiveExamplesSpec WitnessConstStr(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                result[inputState] = example.Value;
            }
            return new DisjunctiveExamplesSpec(result);
        }

        [WitnessFunction("ToUpperCase", 0)]
        internal DisjunctiveExamplesSpec WitnessToUpperCase(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "ToUpperCase";
            var paramIndex = 0;
            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var strs = new HashSet<string>();

                foreach (var output_str in example.Value)
                {
                    var output_string = (string)output_str;
                    if (output_string.All(i => !('a' <= i && i <= 'z')) &&
                        output_string.Any(i => ('a' <= i && i <= 'z') || ('A' <= i && i <= 'Z')))
                    {
                        AddIfNew(inputState, operName, paramIndex, output_string, output_string, ref strs);
                        AddIfNew(inputState, operName, paramIndex, output_string, output_string.ToLower(), ref strs);
                    }

                }
                if (strs.Count == 0) return null;
                result[inputState] = strs;
            }
            return new DisjunctiveExamplesSpec(result);
        }

        [WitnessFunction("ToLowerCase", 0)]
        internal DisjunctiveExamplesSpec WitnessToLowerCase(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "ToLowerCase";
            var paramIndex = 0;

            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var strs = new HashSet<string>();

                foreach (var output_str in example.Value)
                {
                    var output_string = (string)output_str;
                    if (output_string.All(i => !('A' <= i && i <= 'Z')))
                    {
                        AddIfNew(inputState, operName, paramIndex, output_string, output_string, ref strs);
                        AddIfNew(inputState, operName, paramIndex, output_string, output_string.ToUpper(), ref strs);
                    }
                }
                if (strs.Count == 0) return null;
                result[inputState] = strs;
            }
            return new DisjunctiveExamplesSpec(result);
        }




        [WitnessFunction("Concat", 0)]
        internal DisjunctiveExamplesSpec WitnessConcat(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "Concat";

            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var strpairs = new List<Tuple<string, string>>();

                //var output_strings = (HashSet<string>)example.Value;
                foreach (var output_str in example.Value)
                {
                    var output_string = (string)output_str;
                    if (output_string.Length < 2) continue;
                    for (int i = 1; i < output_string.Length; i++)
                    {
                        var str1 = output_string.Substring(0, i);
                        var str2 = output_string.Substring(i);
                        if (!AlreadyHandled(inputState, operName, 0, output_string, str1)
                            && !AlreadyHandled(inputState, operName, 1, output_string, str2))
                            strpairs.Add(Tuple.Create(str1, str2));
                    }
                }
                if (strpairs.Count == 0) return null;
                result[inputState] = strpairs;
            }
            return new DisjunctiveExamplesSpec(result);
        }

        private static List<string> FindAllSubstrings(string s)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < s.Length; i++)
            {
                for (int j = 0; j < s.Length - i; j++)
                {
                    string ss = s.Substring(j, i + 1);
                    list.Add(ss);
                }
            }
            return list;
        }

        [WitnessFunction("Delete", 1)]
        internal DisjunctiveExamplesSpec WitnessDelete(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "Delete";

            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                // the first parameter of Substring is the variable symbol 'x'
                // we extract its current bound value from the given input state
                var src = (string)inputState[rule.Body[0]];
                //int srcLength = src.Length;
                var strList = new List<string>();
                var substringsOfA = FindAllSubstrings(src);
                foreach (var replaced_str in example.Value)
                {

                    var dst = (string)replaced_str;
                    if (src.Length > dst.Length)
                    {
                        if (src.Length == 0 || dst.Length == 0) break;
                        var substringsOfB = FindAllSubstrings(dst);
                        var AExceptSubstrings = substringsOfA.Except(substringsOfB);
                        foreach (var a in AExceptSubstrings)
                        {
                            if (src.Length - a.Length < dst.Length) break;
                            if (dst.Equals(src.Replace(a, "")) && !AlreadyHandled(inputState, operName, 0, dst, a))
                            {
                                strList.Add(a);
                            }


                        }
                    }

                }
                //if (strpairs.Count == 0) return null;
                //result[inputState] = strpairs;

                if (strList.Count == 0) return null;
                result[inputState] = strList.Cast<object>();

            }
            return new DisjunctiveExamplesSpec(result);
        }


        [WitnessFunction("LetDelete", 0)]
        internal DisjunctiveExamplesSpec WitnessLetDelete(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "LetDelete";

            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var output_strs = new HashSet<string>();
                foreach (var s in example.Value) { output_strs.Add((string)s); }
                var strs = new HashSet<string>();
                foreach (var binding in inputState.Bindings)
                {
                    var inputState_strs = new HashSet<string>();
                    if (binding.Value is Object[]) // user inputs
                    {
                        for (int i = 0; i < ((object[])binding.Value).Length; i++)
                        {
                            var val = ((object[])binding.Value)[i];
                            if (val is string) { inputState_strs.Add((string)val); }
                        }
                    }
                    else if (binding.Value is string) // let binding
                    {
                        inputState_strs.Add((string)binding.Value);
                    }
                    else { continue; }

                    foreach (var input_str in inputState_strs)
                    {
                        if (!AlreadyHandled(inputState, operName, 0, output_strs, input_str))
                            strs.Add(input_str);

                    }
                }
                if (strs.Count == 0) return null;
                result[inputState] = strs;
            }
            return new DisjunctiveExamplesSpec(result);
        }



        [WitnessFunction("LetSubstring", 0)]
        internal DisjunctiveExamplesSpec WitnessLetSubstring(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "LetSubstring";

            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var output_strs = new HashSet<string>();
                foreach (var s in example.Value) { output_strs.Add((string)s); }
                var strs = new HashSet<string>();
                foreach (var binding in inputState.Bindings)
                {
                    var inputState_strs = new HashSet<string>();
                    if (binding.Value is Object[]) // user inputs
                    {
                        for (int i = 0; i < ((object[])binding.Value).Length; i++)
                        {
                            var val = ((object[])binding.Value)[i];
                            if (val is string) { inputState_strs.Add((string)val); }
                        }
                    }
                    else if (binding.Value is string) // let binding
                    {
                        inputState_strs.Add((string)binding.Value);
                    }
                    else { continue; }

                    foreach (var input_str in inputState_strs)
                    {
                        if (output_strs.Any(output_str => input_str.Contains(output_str) && input_str.Length > output_str.Length))
                        {
                            if (!AlreadyHandled(inputState, operName, 0, output_strs, input_str))
                                strs.Add(input_str);
                        }
                    }
                }
                if (strs.Count == 0) return null;
                result[inputState] = strs;
            }
            return new DisjunctiveExamplesSpec(result);
        }

        [WitnessFunction("Substring", 1)]
        internal DisjunctiveExamplesSpec WitnessPositionPair(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "Substring";

            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                // the first parameter of Substring is the variable symbol 'x'
                // we extract its current bound value from the given input state
                var x = (string)inputState[rule.Body[0]];

                var occurrences = new HashSet<Tuple<int?, int?>>();
                foreach (var substr in example.Value)
                {
                    var substring = (string)substr;
                    if (substring.Length == 0) continue;
                    // Iterate over all occurrences of 'substring' in 'x',
                    // and add their position boundaries to the list of possible outputs for posPair.
                    for (int i = x.IndexOf(substring);
                         i >= 0;
                         i = x.IndexOf(substring, i + 1))
                    {
                        if (!AlreadyHandled(inputState, operName, 0, substring, i) &&
                            !AlreadyHandled(inputState, operName, 0, substring, i + substring.Length))
                        {
                            occurrences.Add(Tuple.Create((int?)i, (int?)i + substring.Length));
                        }
                    }
                }
                if (occurrences.Count == 0) return null;
                result[inputState] = occurrences;
            }
            return new DisjunctiveExamplesSpec(result);
        }

        [WitnessFunction("AbsolutePosition", 1)]
        internal DisjunctiveExamplesSpec WitnessK(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "AbsolutePosition";

            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var ks = new HashSet<int?>();
                var x = (string)inputState[rule.Body[0]];
                foreach (int? pos in example.Value)
                {
                    if (!AlreadyHandled(inputState, operName, 1, pos, pos) &&
                            !AlreadyHandled(inputState, operName, 1, pos, pos - x.Length - 1))
                    {
                        ks.Add(pos);
                        ks.Add(pos - x.Length - 1);
                    }

                }
                if (ks.Count == 0) return null;
                result[inputState] = ks.Cast<object>();
            }
            return new DisjunctiveExamplesSpec(result);
        }

        static Regex[] UsefulRegexes = {
            new Regex(@"\w+"),  // Word
            new Regex(@"\d+"),  // Number
            new Regex(@"\s+"), // Space
            new Regex(@".+"), // Anything
            new Regex(@"$"), // End of line
            new Regex(@"[-.\p{Lu}\p{Ll}0-9]+@[-.\p{Lu}\p{Ll}0-9]+\.[-.\p{Lu}\p{Ll}0-9]+", RegexOptions.Compiled), //email address
            new Regex(@"", RegexOptions.Compiled), // Epsilon
            new Regex(@"\p{Lu}(\p{Ll})+", RegexOptions.Compiled), // Camel Case
            new Regex(@"\p{Ll}+", RegexOptions.Compiled), // Lowercase word
            new Regex(@"\p{Lu}(\p{Lu})+", RegexOptions.Compiled), // ALL CAPS
            new Regex(@"[0-9]+(\,[0-9]{3})*(\.[0-9]+)?", RegexOptions.Compiled), // Number
            new Regex(@"[-.\p{Lu}\p{Ll}]+", RegexOptions.Compiled), // Words/dots/hyphens
            new Regex(@"[-.\p{Lu}\p{Ll}0-9]+", RegexOptions.Compiled), // Alphanumeric
            new Regex(@"\p{Zs}+", RegexOptions.Compiled), // WhiteSpace
            new Regex(@"\t", RegexOptions.Compiled), // Tab
            new Regex(@",", RegexOptions.Compiled), // Comma
            new Regex(@"\.", RegexOptions.Compiled), // Dot
            new Regex(@":", RegexOptions.Compiled), // Colon
            new Regex(@";", RegexOptions.Compiled), // Semicolon
            new Regex(@"!", RegexOptions.Compiled), // Exclamation
            new Regex(@"\)", RegexOptions.Compiled), // Right Parenthesis
            new Regex(@"\(", RegexOptions.Compiled), // Left Parenthesis
            new Regex(@"""", RegexOptions.Compiled), // Double Quote
            new Regex(@"'", RegexOptions.Compiled), // Single Quote
            new Regex(@"/", RegexOptions.Compiled), // Forward Slash
            new Regex(@"\\", RegexOptions.Compiled), // Backward Slash
            new Regex(@"-", RegexOptions.Compiled), // Hyphen
            new Regex(@"\*", RegexOptions.Compiled), // Star
            new Regex(@"\+", RegexOptions.Compiled), // Plus
            new Regex(@"_", RegexOptions.Compiled), // Underscore
            new Regex(@"=", RegexOptions.Compiled), // Equal
            new Regex(@">", RegexOptions.Compiled), // Greater-than
            new Regex(@"<", RegexOptions.Compiled), // Left-than
            new Regex(@"\]", RegexOptions.Compiled), // Right Bracket
            new Regex(@"\[", RegexOptions.Compiled), // Left Bracket
            new Regex(@"}", RegexOptions.Compiled), // Right Brace
            new Regex(@"{", RegexOptions.Compiled), // Left Brace
            new Regex(@"\|", RegexOptions.Compiled), // Bar
            new Regex(@"&", RegexOptions.Compiled), // Ampersand
            new Regex(@"#", RegexOptions.Compiled), // Hash
            new Regex(@"\$", RegexOptions.Compiled), // Dollar
            new Regex(@"\^", RegexOptions.Compiled), // Hat
            new Regex(@"@", RegexOptions.Compiled), // At
            new Regex(@"%", RegexOptions.Compiled), // Percentage
            new Regex(@"\?", RegexOptions.Compiled), // Question Mark
            new Regex(@"~", RegexOptions.Compiled), // Tilde
            new Regex(@"`", RegexOptions.Compiled), // Back Prime
            new Regex(@"\u2192", RegexOptions.Compiled), // RightArrow
            new Regex(@"\u2190", RegexOptions.Compiled), // LeftArrow
            new Regex(@"(?<=\n)[\p{Zs}\t]*(\r)?\n", RegexOptions.Compiled), // Empty Line
            new Regex(@"[\p{Zs}\t]*((\r)?\n|^|$)", RegexOptions.Compiled), // Line separator


        };

        // For efficiency, this function should be invoked only once for each input string before the learning session starts
        static void BuildStringMatches(string x,
                                        out List<Tuple<Match, Regex>>[] leftMatches,
                                        out List<Tuple<Match, Regex>>[] rightMatches)
        {
            leftMatches = new List<Tuple<Match, Regex>>[x.Length + 1];
            rightMatches = new List<Tuple<Match, Regex>>[x.Length + 1];
            for (int p = 0; p <= x.Length; ++p)
            {
                leftMatches[p] = new List<Tuple<Match, Regex>>();
                rightMatches[p] = new List<Tuple<Match, Regex>>();
            }
            foreach (Regex r in UsefulRegexes)
            {
                foreach (Match m in r.Matches(x))
                {
                    leftMatches[m.Index + m.Length].Add(Tuple.Create(m, r));
                    rightMatches[m.Index].Add(Tuple.Create(m, r));
                }
            }
        }

        [WitnessFunction("RegexPosition", 1)]
        DisjunctiveExamplesSpec WitnessRegexPair(GrammarRule rule, DisjunctiveExamplesSpec spec)
        {
            var operName = "RegexPosition";
            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var x = (string)inputState[rule.Body[0]];
                List<Tuple<Match, Regex>>[] leftMatches, rightMatches;
                BuildStringMatches(x, out leftMatches, out rightMatches);
                var regexes = new List<Tuple<Regex, Regex>>();
                foreach (int? pos in example.Value)
                {
                    foreach (var l in leftMatches[pos.Value])
                    {
                        foreach (var r in rightMatches[pos.Value])
                        {
                            if (!AlreadyHandled(inputState, operName, 11, pos, l.Item2) &&
                                !AlreadyHandled(inputState, operName, 12, pos, r.Item2))
                            {
                                regexes.Add(Tuple.Create(l.Item2, r.Item2));
                            }
                        }

                    }
                    //regexes.AddRange(from l in leftMatches[pos.Value]
                    //             from r in rightMatches[pos.Value]
                    //             select Tuple.Create(l.Item2, r.Item2));
                }
                if (regexes.Count == 0) return null;
                result[inputState] = regexes;
            }
            return new DisjunctiveExamplesSpec(result);
        }

        [WitnessFunction("RegexPosition", 2, DependsOnParameters = new[] { 1 })]
        DisjunctiveExamplesSpec WitnessKForRegexPair(GrammarRule rule, DisjunctiveExamplesSpec spec,
                                             ExampleSpec rrSpec)
        {
            var operName = "RegexPosition";
            var result = new Dictionary<State, IEnumerable<object>>();
            foreach (var example in spec.DisjunctiveExamples)
            {
                State inputState = example.Key;
                var x = (string)inputState[rule.Body[0]];
                var regexPair = (Tuple<Regex, Regex>)rrSpec.Examples[inputState];
                Regex left = regexPair.Item1;
                Regex right = regexPair.Item2;
                var rightMatches = right.Matches(x).Cast<Match>().ToDictionary(m => m.Index);
                var matchPositions = new List<int>();
                foreach (Match m in left.Matches(x))
                {
                    if (rightMatches.ContainsKey(m.Index + m.Length))
                        matchPositions.Add(m.Index + m.Length);
                }
                var ks = new HashSet<int?>();
                foreach (int? pos in example.Value)
                {
                    int occurrence = matchPositions.BinarySearch(pos.Value);
                    if (occurrence < 0) continue;
                    if (!AlreadyHandled(inputState, operName, 2, pos, occurrence) &&
                        !AlreadyHandled(inputState, operName, 2, pos, occurrence - matchPositions.Count))
                    {
                        ks.Add(occurrence);
                        ks.Add(occurrence - matchPositions.Count);
                    }
                }
                if (ks.Count == 0) return null;
                result[inputState] = ks.Cast<object>();
            }
            return new DisjunctiveExamplesSpec(result);
        }


        //[WitnessFunction("Concat", 0)]
        //internal DisjunctiveExamplesSpec WitnessConcat(GrammarRule rule, DisjunctiveExamplesSpec spec)
        //{
        //    var result = new Dictionary<State, IEnumerable<object>>();
        //    foreach (var example in spec.DisjunctiveExamples)
        //    {
        //        State inputState = example.Key;
        //        var sub_str1s = new SortedSet<string>();
        //        var output_strings = (SortedSet<string>)example.Value;
        //        //if (output_strings.Count > 1) continue;
        //        foreach (var output_string in output_strings)
        //        {
        //            for (int i = 1; i < Math.Min(9, output_string.Length); i++)
        //            {
        //                if (!sub_str1s.Contains(output_string.Substring(0, i)))
        //                {
        //                    sub_str1s.Add(output_string.Substring(0, i));
        //                }
        //            }
        //        }
        //        if (sub_str1s.Count == 0) return null;
        //        result[inputState] = sub_str1s.Cast<object>();
        //    }
        //    return new DisjunctiveExamplesSpec(result);
        //}

        //[WitnessFunction("Concat", 1, DependsOnParameters = new[] { 0 })]
        //internal DisjunctiveExamplesSpec WitnessConcat2(GrammarRule rule, DisjunctiveExamplesSpec spec, DisjunctiveExamplesSpec s1_spec)
        //{
        //    var result = new Dictionary<State, IEnumerable<object>>();
        //    foreach (var example in spec.DisjunctiveExamples)
        //    {
        //        State inputState = example.Key;
        //        var output_strings = (SortedSet<string>)example.Value;
        //        var sub_str1s = (SortedSet<string>)s1_spec.DisjunctiveExamples[inputState];
        //        var sub_str2s = new SortedSet<string>();
        //        foreach (var output_string in output_strings)
        //        {
        //            foreach (var sub_str1 in sub_str1s)
        //            {
        //                //if (sub_str1.Length == 0) continue;
        //                if (output_string.StartsWith(sub_str1, StringComparison.Ordinal) && !output_string.Equals(sub_str1))
        //                {
        //                    var sub_str2 = output_string.Substring(sub_str1.Length);
        //                    if (sub_str2.Length > 0)
        //                    {
        //                        sub_str2s.Add(sub_str2);
        //                    }
        //                }
        //            }
        //        }
        //        if (sub_str2s.Count == 0) return null;
        //        result[inputState] = sub_str2s.Cast<object>();
        //    }
        //    return new DisjunctiveExamplesSpec(result);
        //}

    }
}
