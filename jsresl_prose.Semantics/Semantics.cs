using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace jsresl_prose.Semantics
{
    public static class Semantics
    {
        public static string ConstStr(string s)
        {
            return s;
        }

        public static string Nth_str(Object[] vs, int k)
        {
            if (k < vs.Length && k >= 0 && vs[k] is string) { return (string)vs[k]; }
            return null;
        }

        public static string Concat(Tuple<string, string> strPair)
        {
            return strPair.Item1 + strPair.Item2;
        }

        public static string ToUpperCase(string s)
        {
            return s.ToUpper();
        }

        public static string ToLowerCase(string s)
        {
            return s.ToLower();
        }

        public static string Delete(string x, string str)
        {
            //Console.WriteLine("delete 펑션을 지나가긴 합니다..!");
            return x.Replace(str, "");
        }


        public static string Substring(string x, Tuple<int?, int?> posPair)
        {
            if (posPair.Item1 == null || posPair.Item2 == null)
                return null;
            int start = posPair.Item1.Value;
            int end = posPair.Item2.Value;
            if (start < 0 || start >= x.Length ||
                end < 0 || end > x.Length || end < start)
                return null;
            return x.Substring(start, end - start);
        }

        public static int? AbsolutePosition(string x, int k)
        {
            if (k > x.Length || k < -x.Length - 1)
                return null;
            return k >= 0 ? k : (x.Length + k + 1);
        }

        public static int? RegexPosition(string x, Tuple<Regex, Regex> regexPair, int occurrence)
        {
            if (regexPair.Item1 == null || regexPair.Item2 == null)
                return null;
            Regex left = regexPair.Item1;
            Regex right = regexPair.Item2;
            var rightMatches = right.Matches(x).Cast<Match>().ToDictionary(m => m.Index);
            var matchPositions = new List<int>();
            foreach (Match m in left.Matches(x))
            {
                if (rightMatches.ContainsKey(m.Index + m.Length))
                    matchPositions.Add(m.Index + m.Length);
            }
            if (occurrence >= matchPositions.Count ||
                occurrence < -matchPositions.Count)
                return null;
            return occurrence >= 0
                ? matchPositions[occurrence]
                : matchPositions[matchPositions.Count + occurrence];
        }
    }
}