# Jsresl Synthesizer 
An inductive program synthesizer for JavaScript implemented atop [PROSE](https://www.microsoft.com/en-us/research/project/prose-framework/) framework
## Structure of Jsresl Synthesizer
The file `jsresl_prose.grammar` describes the underlying grammar for the target DSL. The current DSL is similar to the one for [FlashFill](https://support.microsoft.com/en-us/office/using-flash-fill-in-excel-3f9bcf1e-db93-4890-94a0-1578341f73f7). Thus the synthesizer can generate only string manipulating programs.  
The file `WitnessFunctions.cs` describes inverse semantics for string manipulating operators. Semantics.cs defines semantics of the operators. `Program.cs` contains the main function. The user intension is expected to be given in the form of I/O examples (strings). It finds the smallest DSL program through the ranking function defined in `RankingScore.cs`. It converts the best DSL program to JavaScript program through the transformation functions in `ASTTransformer.cs`.
```
.
├── jsresl_prose
│   ├── ASTTransformer.cs
│   ├── NUnitTestClass.cs
│   ├── Program.cs
│   └── jsresl_prose.grammar
├── jsresl_prose.Learning
│   ├── RankingScore.cs
│   └── WitnessFunctions.cs
└── jsresl_prose.Semantics
    └── Semantics.cs
```
## Build and Run Jsresl Synthesizer
To build the project, use [Visual Studio](https://visualstudio.microsoft.com/vs/).
```sh
$ cd jsresl_prose/bin/Debug/
$ mono jsresl_prose.exe
```
## How to adjust the number of expression repeats
By modifying the value of a variable `threshold`, you may limit the depth of "unrolled" grammar. 
```csharp
private int threshold = ...
```

## Reproducing the experimental results in the paper
Use Visual Studio Unit test pads.

## Example of running jsresl_prose
Here is an example of the running of jsresl_prose synthesizer.
```
$ mono jsresl_prose.exe
Provide a new input-output examples (If You have done, press [control] + [D]
inputs (e.g. "Jo" "jeongmin" ): "please delete <answer> there" "5 * 6 -7 = answer23"
output (e.g. "Jo jeongmin" ): "5 * 6 -7 = 23"
inputs (e.g. "Jo" "jeongmin" ): "delete <blank> left sentence" "3 * 5 - blank5 = 10"
output (e.g. "Jo jeongmin" ): "3 * 5 - 5 = 10"
inputs (e.g. "Jo" "jeongmin" ): // press [control] + [D]
|Number of all programs|: 1320
[score = 0.000] ToLowerCase(let x = Nth_str(vs, 1) in Delete(x, ToLowerCase(ToUpperCase(let x = Nth_str(vs, 1) in Substring(x, PositionPair(RegexPosition(x, RegexPair(/\\s+/, /\\w+/), 1), RegexPosition(x, RegexPair(/\\p{Ll}+/, /[0-9]+(\\,[0-9]{3})*(\\.[0-9]+)?/), 0)))))))
======= Javascript program ======= 
function test(vs){ vs[1].replace(vs[1].slice(Array.from(vs[1].matchAll(/(\s+)(\w+)/g),_tmp=>_tmp.index+_tmp[1].length).slice(1)[0],Array.from(vs[1].matchAll(/(\p{Ll}+)([0-9]+(,[0-9]{3})*(\.[0-9]+)?)/gu),_tmp=>_tmp.index+_tmp[1].length).slice(0)[0]).toUpperCase().toLowerCase(), "" ).toLowerCase() }
```
